﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using LitJson;
using System;
using UnityEngine.UI;
using Assets;

/*HOW IT WORKS:
 *  -Accepts a path to a JSON file (path should be a string)
 *  -Uses the path to fetch the JSON file, and then maps it to a dictionary
 *  -Data is only parsed once when the graph is opened. If changes occur, the graph must be closed and re-opened again.
 * 
 */
public class MakeGraph : MonoBehaviour
{
    string[] paths;
    protected Graph graphData;
    int simulateDayCount;

    public LineRenderer[] graphLines;
    public TextMesh CompanyText;
    public TextMesh beginDay;
    public TextMesh endDay;
    public TextMesh hiVal;
    public TextMesh loVal;
    public int numOfLines;
    public Boolean follow;

    // Use this for initialization
    void Start()
    {
        numOfLines = 4;
        follow = true;
        paths = new string[numOfLines];
        paths[0] = Application.streamingAssetsPath + "/AAPL_stock_data.json";
        paths[1] = Application.streamingAssetsPath + "/FB_stock_data.json";
        paths[2] = Application.streamingAssetsPath + "/WMT_stock_data.json";
        paths[3] = Application.streamingAssetsPath + "/NTDOF_stock_data.json";
        graphData = new Graph(numOfLines, paths);
        simulateDayCount = 0;   //0 indicates the first data point that we currently have

        Vector3 cameraVector = Camera.main.transform.forward;

        transform.position = Camera.main.transform.position + Camera.main.transform.forward * 10;
        transform.LookAt(Camera.main.transform);
        PopulateGraph(simulateDayCount);
        
    }

    // Update is called once per frame
    void Update()
    {
        //Have the graph continuously look at the camera as it moves around
        if (follow)
            transform.LookAt(Camera.main.transform);
    }

    /*
     * This function takes care of making changes to the 3D elements that are seen in unity.
     * It mostly interacts with positioning of the vertices for the data lines and the texts
     * that indicate the starting/ending day and the high/low values.
     */
    public void PopulateGraph(int startDay)
    {
        //if all data graphs are toggled off, simply deactivate all line graphs
        if (graphData.getCurrentActive() == 0)
        {
            for (int i = 0; i < graphData.getNumberOfLines(); i++)
                graphLines[i].gameObject.SetActive(false);
        }
        else //there is at least one line that is active, so keep updating the line
        {
            graphData.setBounds(startDay);      //determine the upper/lower limit of the graph
            graphData.calcYCoords(startDay);    //determine the coordinates relative to the graph for each data
                                                //  point that is visible

            //populate the text fields
            hiVal.text = graphData.getHiBound().ToString();
            loVal.text = graphData.getLoBound().ToString();
            CompanyText.text = graphData.getCompanyName(1);
            beginDay.text = graphData.getDate(startDay);
            endDay.text = graphData.getDate(startDay + 11);

            //iterate though all the lines
            for (int i = 0; i < graphData.getNumberOfLines(); i++)
            {
                //checks if the line is supposed to be active
                if (graphData.lineIsActive(i))
                {
                    graphLines[i].gameObject.SetActive(true);   //in case it was previously inactive, activate it
                    for (int j = 0; j < 11; j++)                //proceed to update its data point locations on the graph
                    {
                        graphLines[i].SetPosition(j, new Vector3(0, graphData.getYCoordinate(i, j), graphLines[i].GetPosition(j).z));
                    }
                }
                else  //if the current line is supposed to be inactive but isn't, deactivate it
                    graphLines[i].gameObject.SetActive(false);
            }
        }
    }

    // Moves the range of 11 data points to include the next one in line and drop the first one
    public void ShiftGraphRight()
    {
        if (simulateDayCount < graphData.getLimitOfDays()-12)
            PopulateGraph(++simulateDayCount);
    }

    // Moves the range of 11 data points to include the previous one in line and drop the last one
    public void ShiftGraphLeft()
    {
        if (simulateDayCount > 0)
            PopulateGraph(--simulateDayCount);
    }

    //handles activation/deactivation of lines by calling Graph's changeVisibility function
    //  as well as refreshing the graph to reflect the changes.
    public void toggleLineActive(int index)
    {
        graphData.changeLineVisibility(index);
        PopulateGraph(simulateDayCount);
    }

}

/*goals for next week
-coolify it.
-make into a prefab, and the prefab should accept a static json file
-add more functionality: slider, should be able to open up various amounts of graphs, enable click events on each one

Expectations:
    - I will require the server guys give me a path to the JSON file

Things i added:
    - I added the functionality to support up to 10 graphs
    - Coolified(?) it somewhat using the Unity Sample UI assets
    - Split the giant file of code into separate files to be more modular
    - Added the ability to 'deactivate' particular graphs at runtime and having the graph resize based on the
        narrower amount of data and vice versa.
    - Added the ability for the graph to look at the camera as it moves around (toggleable)
    - Added documentation

Things i want to add:
    - mouseover tooltips for the lines that give the name of the company, as well as for the individual vertices to give
        the stock price.

Things to note:
    - I can say that I have done research as an undergrad at SDSU under professor Price
    - git cache flush for committing, use richard's gitignore

*/