﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using LitJson;

namespace Assets
{
    [System.Serializable]
    public class Graph
    {
        private static int MAX_SIZE = 10;   //we will only accept up to 10 lines
        private CompanyStock[] lines;       //holds our Stock data
        private double[,] Ycoords;          //private 2D array that will be used to calculate coordinates for our data
        private Boolean[] lineIsVisible;    //private array that keeps track of which lines should be visible

        private double lowBound = 0.0f;     //keeps track of the current minimum value of all data points within a range
        private double hiBound = 0.0f;      //keeps track of the current maximum value of all data points within a range
        private int size = 0;               //the actual number of lines that our graph has
        private int currentActive = 0;      //the number of graphs that are currently visible

        public Graph(int numOfInput, string[] paths)
        {
            //check to see if our input is of valid length
            if (numOfInput == paths.Length && numOfInput < MAX_SIZE)
            {
                size = numOfInput;
                lines = new CompanyStock[size];
                Ycoords = new double[size,11];
                lineIsVisible = new Boolean[size];
                for (int i = 0; i < size; i++)
                {
                    GenerateData(i, paths[i]);
                    lineIsVisible[i] = true;
                    currentActive++;
                }
            }
        }

        /*
         * Scans the range of data points starting from a particular day(index) to 11 days after since
         *  we have 11 data points that we can show on the graph. Searches for the local max/min amongst
         *  all lines.
         */
        public void setBounds(int startDay)
        {
            //Set our base max/min equal to the first value of the first line that is active
            int firstActiveLine = findFirstActiveLine();
            double hi = lines[firstActiveLine].StockData[startDay].value;
            double lo = lines[firstActiveLine].StockData[startDay].value;

            //iterate through the remaining data points
            for (int i = firstActiveLine; i < size; i++)
            {
                //only scan the data points of lines that are active
                if (lineIsVisible[i])
                {
                    for (int j = startDay; j < startDay + 11; j++)
                    {
                        if (lines[i].StockData[j].value > hi)
                            hi = lines[i].StockData[j].value;
                        if (lines[i].StockData[j].value < lo)
                            lo = lines[i].StockData[j].value;
                    }
                }
            }

            lowBound = lo - 10f;
            if (lowBound < 0)
                lowBound = 0;
            hiBound = hi + 10f;
        }

        /*
         * Based on the result from setBounds(), we now calculate the relative position each data point
         * and set it inside our YCoords array.
         */
        public void calcYCoords(int startDay)
        {
            double range = hiBound - lowBound;

            //iterate through our lines
            for (int i = 0; i < size; i++)
            {
                //only take into account those that are active
                if (lineIsVisible[i])
                {
                    for (int j = 0; j < 11; j++)
                    {
                        double difference = lines[i].StockData[j + startDay].value - lowBound;  //diff = value - lowestNum
                        double relativeDifference = difference / range;                         //obtain difference ratio
                        Ycoords[i, j] = (6.0f * relativeDifference) - 3.0f;                     //fit coordinate to graph
                    }
                }
            }
        }

        /*
         * Takes in the path to the .json files and parses through them for the relevant information.
         * Then places the information into our array of CompanyStock items.
         */
        private void GenerateData(int index, string path)
        {
            string jsonString = File.ReadAllText(path);
            JsonData Stocks = JsonMapper.ToObject(jsonString);

            Data[] stockData = new Data[Stocks["Stock Data"].Count];
      
            CompanyStock individualLine = new CompanyStock();
            individualLine.CompanyName = Stocks["Company"].ToString();
            individualLine.Symbol = Stocks["Symbol"].ToString();
        
            //iterate thought eh Data array and populate the price, month, and day for each index
            for (int j = 0; j < Stocks["Stock Data"].Count; j++)
            {
                stockData[j] = new Data();
                stockData[j].value = (double)Stocks["Stock Data"][j]["Stock Value"];
                stockData[j].month = Stocks["Stock Data"][j]["Month"].ToString();
                stockData[j].day = (int)Stocks["Stock Data"][j]["Day"];
            }
            //after the Companystock item is creates, assign it to our lines array.
            individualLine.StockData = stockData;
            lines[index] = individualLine;

        }

        //Finds the first line that is active
        public int findFirstActiveLine()
        {
            int firstActiveLine = 0;
            while (!lineIsVisible[firstActiveLine])
                firstActiveLine++;
            return firstActiveLine;
        }

        //Checks if the indexed line is currently supposed to be active
        public Boolean lineIsActive(int index)
        {
            return lineIsVisible[index];
        }

        //Changes the boolean value of the indexed line to the opposite value.
        //If a line is toggled off, we subtract from currentActive.
        //If a line is toggled on, increment.
        public void changeLineVisibility(int index)
        {
            if (lineIsVisible[index])
                currentActive--;
            else
                currentActive++;
            lineIsVisible[index] = !lineIsVisible[index];
        }

        //PUBLIC GET COMMANDS
        public int getCurrentActive()
        {
            return currentActive;
        }

        public double getHiBound()
        {
            return hiBound;
        }

        public double getLoBound()
        {
            return lowBound;
        }

        public string getCompanyName(int index)
        {
            return lines[index].CompanyName + " (" + lines[index].Symbol + ")";
        }

        public string getDate(int date)
        {
            return lines[0].StockData[date].month + " " + lines[0].StockData[date].day;
        }

        public float getYCoordinate(int line, int day)
        {
            return (float)Ycoords[line, day];
        }

        public int getLimitOfDays()
        {
            return lines[0].StockData.Length;
        }

        public int getNumberOfLines()
        {
            return size;
        }

    }
}
