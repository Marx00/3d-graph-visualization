﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    // Use this for initialization
    void Start () {

    }

    // Update is called once per frame
    void Update() {

        //First section is WASD movement of the camera
        if (Input.GetKey(KeyCode.W))
        {
            Vector3 move = new Vector3(0, 0, 0.1f);
            transform.Translate(move, Space.Self);
        }

        if (Input.GetKey(KeyCode.A))
        {
            Vector3 move = new Vector3(-0.1f, 0, 0);
            transform.Translate(move, Space.Self);
        }

        if (Input.GetKey(KeyCode.S))
        {
            Vector3 move = new Vector3(0, 0, -0.1f);
            transform.Translate(move, Space.Self);
        }

        if (Input.GetKey(KeyCode.D))
        {
            Vector3 move = new Vector3(0.1f, 0, 0);
            transform.Translate(move, Space.Self);
        }

        if (Input.GetKey(KeyCode.R))
        {
            Vector3 move = new Vector3(0, 0.1f, 0);
            transform.Translate(move, Space.Self);
        }

        if (Input.GetKey(KeyCode.F))
        {
            Vector3 move = new Vector3(0, -0.1f, 0);
            transform.Translate(move, Space.Self);
        }


        //Second section is the rotation of the camera using arrow keys
        if (Input.GetKey(KeyCode.RightArrow))
        {
            Camera.main.transform.Rotate(0, 1f, 0);
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            Camera.main.transform.Rotate(0, -1f, 0);
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            Camera.main.transform.Rotate(1f, 0, 0);
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            Camera.main.transform.Rotate(-1f, 0, 0);
        }
    }
}
