﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets
{

    [System.Serializable]
    public class CompanyStock
    {
        public string CompanyName;
        public string Symbol;
        public Data[] StockData;

        public CompanyStock()
        {
            CompanyName = "";
            Symbol = "";
            StockData = null;
        }

    }

    [System.Serializable]
    public class Data
    {
        public string month;
        public int day;
        public double value;

        public Data()
        {
            month = "";
            day = 0;
            value = 0.0f;
        }
    }
}
