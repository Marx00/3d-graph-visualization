﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateAttempt : MonoBehaviour {

    public Transform StockGraph;

	// Use this for initialization
	void Start () {
        Instantiate(StockGraph, new Vector3(0, 0, 0), Quaternion.identity);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
